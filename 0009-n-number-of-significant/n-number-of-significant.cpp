#include <stdio.h>

//Функція яка знаходить суму цифр для числа n
int sum_num(int n) {
	int result = 0, c = 0;

	while (n >= 1) {
		c = n % 10;
		n /= 10;
		result += c;
	}
	return result;
}

//Функція яка знаходить добуток цифр для числа n
int product_num(int n) {
	int result = 1, c = 0;

	while (n >= 1) {
		c = n % 10;
		if (c == 0) 
			return 0;
		n /= 10;
		result *= c;
	}
	return result;
}

int min = 0;

//Перебіраємо всі n-значні числа і знаходимо ті з них сума цифр яких дорівнює їх добутку
int num(int n) {
	int rozryad = 1, result = 0, ind = 0;
	
	//Визначаємо початкове n-значне число 
	for (int i = 1; i < n; i++)
		rozryad *= 10;
	
	//Перебіраємо всі можливі n-значні числа
	for (int i = rozryad; i <= 9.9999999999 * rozryad; i++)
	//Якщо сума цифр біжучого числа дорівнює їхньому добутку то лічильник збільшити на одиницю (при цьому перше таке число запам'ятовуємо у глобальну змінну min)
	if (sum_num(i) == product_num(i)) {
		result++;
		if (ind == 0 ) {
			min = i;
			ind = 1;
		}
	}
		
	//	printf("%d\n",i);
	return result;
		
};



int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	int n,x,count = 0;

	fscanf(f1,"%d",&n);


//	x = sum_num(12432);
//	x = product_num(12121);
	if (n == 1)
		fprintf(f2,"10 0\n");
	else {
		count = num(n);	
		fprintf(f2,"%d %d\n",count,min);
	};

	fclose(f1);
	fclose(f2);
	return 0;
}
