#include <stdio.h>


int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");
	
	int n, count = 0;
	float number, sum = 0;

	fscanf(f1,"%d",&n);
	for (int i = 1; i <=n; i++) {
		fscanf(f1,"%f",&number);
		if (number < 0) {
			sum += number;
			count++;
		}
	}


	fprintf(f2,"%d %.2f\n",count,sum);

	fclose(f1);
	fclose(f2);
	return 0;
}
