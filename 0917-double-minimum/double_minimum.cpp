#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int length = 0, i = 0;
	float min = 34000000000.0f, array[100], result;

	fscanf(inf,"%d",&length);
	
	
	while (!feof(inf)) {
		if (i >= length) 
			break;
		fscanf(inf,"%f",&array[i]);
		if (min > array[i]) 
			min = array[i];
		i++;
	}
	
	result = min + min;
	fprintf(outf,"%.2f\n",result);

	fclose(inf);
	fclose(outf);
	return 0;
}
