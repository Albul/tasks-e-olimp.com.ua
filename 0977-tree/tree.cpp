#include <stdio.h>
#include <memory.h>
#define MAX 102
int m[MAX][MAX], used[MAX], order[MAX], count = 0, count_d = 0;
int a, b;
int n, r;

//����� � �������
void dfs(int v) {
	int i;
	used[v] = 1;
	order[count_d] = v;
	count_d++;
	for(i = 1; i <= n; i++)
		if(m[v][i] && !used[i])
			dfs(i);
}

int main() {
	memset(m,0,sizeof(m));
	memset(used,0,sizeof(used));

	FILE *f1, *f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");

	//������� ���� ������� �������� ��������
	fscanf(f1,"%d",&n);
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			fscanf(f1,"%d",&a);
			m[i][j] = a;
		}
	};
	
	//ϳ��������� ������� ����� � �����
	count = 0;
	for (int i = 1; i <= n; i++) {
		for (int j = i + 1; j <= n; j++) {
			fscanf(f1,"%d",&a);
			if (m[i][j])
				count++;
		}
	};
	

	//�������� ���� ������� � �������
	int count_use = 0;
	dfs(1);
	//ϳ��������� ������� �������� ������
	for (int i = 1; i <= n; i++) {
		if (used[i]) 
			count_use++;
	};
	

	//���� ������� ����� �� ��. ����� �� ������� ������, � �� ������� ������� ��� ����� � �������, �����
	//���� ������� - �� ���� ������
	if (((n - 1) == count) && (count_use == n))
		fprintf(f2,"YES\n");
	else
		fprintf(f2,"NO\n");
	

	//getchar();
	fclose(f1);
	fclose(f2);
	return 0;
}
