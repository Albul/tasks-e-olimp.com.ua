#include <iostream>
#include <fstream>
#include "string.h";
using namespace std;

int main() {
	ifstream f1("input.txt");
	ofstream f2("output.txt");

	int x = 0;
	char str[15];

	f1 >> x;
	if (x <= 3) 
		strcpy(str,"Initial");
	if (x > 3 && x <= 6)
		strcpy(str,"Average");
	if (x > 6 && x < 10)
		strcpy(str,"Sufficient");
	if (x >= 10 && x <= 12)
		strcpy(str,"High");
	
	f2 << str << endl;

	f1.close();
	f2.close();
	return 0;
}
