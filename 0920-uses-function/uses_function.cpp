#include <stdio.h>

float max(float x, float y) {
	if (x > y)
		return x;
	else
		return y;
}	

//������� ���������� ������� �� ���� �����
float min(float x, float y) {
	if (x < y)
		return x;
	else
		return y;
}

//����������� �-� ���������� ������� �� ����� �����
float min(float x, float y, float z) {
	if (min(x,y) < min(y,z))
		return min(x,y);
	else
		return min(y,z);
};

int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");

	float x, y, z, result;

	fscanf(f1,"%f%f%f",&x,&y,&z);

	result = min(max(x,y),max(y,z), x+y+z);

	fprintf(f2,"%.2f\n",result);

	fclose(f1);
	fclose(f2);
	return 0;
}