#include <stdio.h>
#include <math.h>


//Обчислити площу трикутника за формулою Герона
float heron(int a, int b, int c) {
	float p, result;
	p = (float)(a + b + c)/2;
	result = (float)sqrt(p * (p - a) * (p - b) * (p - c));
	return result;
};

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");
	
	int a,b,c;
	float s,ha,hb,hc;

	fscanf(f1,"%d%d%d",&a,&b,&c);

	s = heron(a,b,c);
	ha = (float) 2*s/a;
	hb = (float) 2*s/b;
	hc = (float) 2*s/c;

	fprintf(f2,"%.2f %.2f %.2f\n",ha,hb,hc);
	
	fclose(f1);
	fclose(f2);
	return 0;
}

