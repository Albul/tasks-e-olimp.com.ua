#include <stdio.h>

int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");
	
	int n = 0, sum = 0;

	fscanf(f1,"%d",&n);

	while (n) {
		if (n - 100 >= 0) {
			n -= 100;
			sum += 100;
		}
		else {
			if (n - 20 >= 0) {
				n -= 20;
				sum += 30;
			}
			else {
				if (n - 1 >= 0) {
					n -= 1;
					sum += 2;
				}
				else
					break;
			};
		};
	};
	
	fprintf(f2,"%d\n",sum);

	fclose(f1);
	fclose(f2);
	return 0;
}