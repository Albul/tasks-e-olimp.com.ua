#include <stdio.h>
#include <math.h>


//�-� ����������� ������� ������ �� ����� �������
float segment(float x1, float y1, float x2, float y2) {
	return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
};

int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");

	float x1,y1,x2,y2,x3,y3,p,s,d1,d2,d3,p2;

	fscanf(f1,"%f%f%f%f%f%f",&x1,&y1,&x2,&y2,&x3,&y3);
	
	//���������� ������� ���� �����
	d1 = segment(x1,y1,x2,y2);
	d2 = segment(x1,y1,x3,y3);
	d3 = segment(x2,y2,x3,y3);

	//��������� ��������, ���������� �� �����
	p = d1 + d2 + d3;
	p2 = p/2;
	s = sqrt(p2*(p2-d1)*(p2-d2)*(p2-d3));
	
	fprintf(f2,"%.4f %.4f\n",p,s);

	fclose(f1);
	fclose(f2);
	return 0;
}
