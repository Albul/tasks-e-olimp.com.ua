#include <stdio.h>
#define abs(x) ((x < 0)? -x : x)
int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int length, i = 0;
	float array[101];
	//Змінна початковий максимум
	float max = -340000000000000000000000000.0f;
	fscanf(inf,"%d",&length);
	while (i < length) {
		fscanf(inf,"%f",&array[i]);
		//Якщо максимум менший за елемент масива то, змінити максимум
		if (max < array[i]) {
			max = array[i];
		};
		i++;
	};

	//Знайти модуль
	max = (max < 0)? -max : max;
	fprintf(outf,"%.2f\n",max);

	fclose(inf);
	fclose(outf);
	return 0;
}
