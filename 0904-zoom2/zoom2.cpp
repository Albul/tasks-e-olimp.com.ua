#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int i,j = 0, array[100];

	fscanf(inf,"%d",&i);
	//Поки не кінець файла, зчитуємо з нього цілі числа і записуємо в масив
	//(feof(inf) == 0)? true : false
	while (!feof(inf)) {
		fscanf(inf,"%d",&array[j]);
		//Якщо біжуче число невід'ємне то збільшити його на одиницю
		if (array[j] >= 0) 
			array[j] += 2;
		j++;
	};
	
	//Виводимо через прогалину всі числа масиву крім останнього
	for (j = 0; j < i-1; j++)
		fprintf(outf,"%d ",array[j]);
	
	//Виводимо останнє числоі
	fprintf(outf,"%d\n",array[j]);

	fclose(inf);
	fclose(outf);
	return 0;
}
