#include <iostream>
#include <fstream>
using namespace std;

//Факторіал
double fact(int x) {
	double result = 1;

	for(int i = 1; i <= x ; i++) {
		result *= i;		
	}
	return result;
}

int main() {
	ifstream f1("input.txt");
	ofstream f2("output.txt");

	int x = 0;
	double y;
	f1 >> x;
	
	y = fact(x)/fact(x-3);

	f2 << fact(x) << endl;
	

	f1.close();
	f2.close();
	return 0;
}
