#include <stdio.h>


int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");
	int a,m, i = 0, count = 0;

	fscanf(f1,"%d%d",&a,&m);
	
	while (m) {
		m -= (a + i);
		count++;
		if ((a + i + 1) * 2 == m)
			break;
		i++;	
	}

	if (a != m) 
		count++;

	fprintf(f2,"%d\n",count);
	fclose(f1);
	fclose(f2);
	return 0;
}