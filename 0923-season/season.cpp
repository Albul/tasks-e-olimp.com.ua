#include <stdio.h>

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");

	int i;

	fscanf(f1,"%d",&i);


	//Розумний перемикач із діапазонами
	switch (i) {
	case 12:
	case 1:
	case 2:
		fprintf(f2,"Winter\n");
		break;
	case 3:
	case 4:
	case 5:
		fprintf(f2,"Spring\n");
		break;
	case 6:
	case 7:
	case 8:
		fprintf(f2,"Summer\n");
		break;
	case 9:
	case 10:
	case 11:
		fprintf(f2,"Autumn\n");
		break;
	};


/*
	//Або ось так
	if (i == 12 || i == 1 || i == 2)
		fprintf(f2,"Winter\n");
*/
	fclose(f1);
	fclose(f2);
	return 0;
}
