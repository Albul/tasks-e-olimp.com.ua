#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int length, i = 0, j = 0;
	float array[100], sum = 0, average = 0;
	fscanf(inf,"%d",&length);
	while (i < length) {
		fscanf(inf,"%f",&array[i]);
		//Якщо елемент масиву додатній, то додати його до загальної суми, і збільшити лічильник на одиницю
		if (array[i] > 0) {
			sum += array[i];
			j++;
		};
		i++;
	};
	
	//Якщо додатні елементи були у масиві то вирахувати їх середнє, в противному разі вивести повідомлення "Not Found"
	if (j > 0 ) {
		average = sum / j;
		fprintf(outf,"%.2f\n",average);
	}
	else
		fprintf(outf,"Not Found\n");
	

	fclose(inf);
	fclose(outf);
	return 0;
}
