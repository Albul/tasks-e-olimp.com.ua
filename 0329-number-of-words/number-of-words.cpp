#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");

	char s[1000];
	int count = 0, length;
	
	fgets(s,1000,f1);
	rewind(f1);
	length = strlen(s);
//	sscanf();
	while (!feof(f1)) {
		fscanf(f1,"%s",&s);
		count++;
	}
	
	if (isspace(s[length-1]))
		count--;

//	getchar();
	fprintf(f2,"%d\n",count);

	fclose(f1);
	fclose(f2);
	return 0;
}
