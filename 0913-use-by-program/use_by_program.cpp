#include <stdio.h>

float sum(float a, float b) {
	return a+b;
};

float product(float a, float b) {
	return a*b;
};

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");

	int length;
	float summ = 0, prod = 0;

	fscanf(f1,"%d",&length);
	
	//Оголошуємо покажчик на дійсне, і виділяємо місце в пам'яті під масив дійсних в двічі більший за кількість пар (+1 значить що ми будемо масив нумерувать поч. з одиниці)
	//float* arr = new float[length*2+1];
	float arr[2][1000];

/*
	//Нумерую масив із одиниці, тому виділяю йому місце на 1 більше
	for (int i = 1; i < length*2+1; i++) {
		fscanf(f1,"%f",&arr[i]);
	}
	
	for (int i = 1; i < length*2+1; i++) {
		if ((i % 2) != 0) {
			summ = sum(arr[i],arr[i+1]);
			prod = product(arr[i],arr[i+1]);
			fprintf(f2,"%.4f %.4f\n",summ,prod);
		}
	};
*/
	//Потрібно обов'язково звільнить місце під масивом
	//delete [] arr;

	for (int i = 0; i < length; i++) {
		fscanf(f1,"%f%f",&arr[0][i],&arr[1][i]);
		summ = sum(arr[0][i],arr[1][i]);
		prod = product(arr[0][i],arr[1][i]);
		fprintf(f2,"%.4f %.4f\n",summ,prod);
	};

	fclose(f1);
	fclose(f2);
	return 0;
}
