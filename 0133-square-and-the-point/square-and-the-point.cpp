#include <stdio.h>

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	int n,result;
	fscanf(f1,"%d",&n);

	result = (n+1)*(n+1);

	fprintf(f2,"%d\n",result);

	fclose(f1);
	fclose(f2);
	return 0;
}
