#include <stdio.h>

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	
	int a,b,c;

	fscanf(f1,"%d%d%d",&a,&b,&c);
	if ((a > b && a < c) || (a < b && a > c)) 
		fprintf(f2,"%d\n",a);
	if ((b > a && b < c) || (b < a && b > c))
		fprintf(f2,"%d\n",b);
	
	if ((c > a && c < b) || (c < a && c > b))
		fprintf(f2,"%d\n",c);
	
	fclose(f1);
	fclose(f2);
	return 0;
}
