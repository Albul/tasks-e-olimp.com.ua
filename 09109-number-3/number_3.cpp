#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int length = 0, i = 0, count = 0;
	float array[100], sum = 0;

	fscanf(inf,"%d",&length);
	
	
	while (!feof(inf)) {
		if (i >= length) 
			break;
		fscanf(inf,"%f",&array[i]);
		if ((((i+1) % 3) == 0) && (array[i] > 0)) {
			sum += array[i];
			count++;
		}
		i++;
	}
	
	fprintf(outf,"%d %.2f\n",count,sum);

	fclose(inf);
	fclose(outf);
	return 0;
}

