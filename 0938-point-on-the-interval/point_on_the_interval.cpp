#include <stdio.h>

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");

	float x1,y1,x2,y2,x,y,alpha;

	fscanf(f1,"%f%f%f%f%f",&x1,&y1,&x2,&y2,&alpha);
	

	//Обчислити координати точки
	x = (x1 + alpha*x2)/(1+alpha);
	y = (y1 + alpha*y2)/(1+alpha);
	
	fprintf(f2,"%.2f %.2f\n",x,y);

	fclose(f1);
	fclose(f2);
	return 0;
}
