#include <stdio.h>
#include <math.h>


//Обчислити площу трикутника за формулою Герона
float heron(float a, float b, float c) {
	float p, result;
	p = (a + b + c)/2;
	result = sqrt(p * (p - a) * (p - b) * (p - c));
	return result;
};

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");
	
	float a,b,c,d,f;
	float s1 = 0.0f,s2 = 0.0f,result = 0.0f;

	fscanf(f1,"%f%f%f%f%f",&a,&b,&c,&d,&f);

	s1 = heron(d,c,f);
	s2 = heron(a,b,f);
	result = s1 + s2;

	fprintf(f2,"%.4f\n",result);
	
	fclose(f1);
	fclose(f2);
	return 0;
}
