#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string.h>
using namespace std;

void strtoarr(char *str, int *T) {
	//� ������� ������� ���������� ������� �����
	T[0] = strlen(str);
	//����������� �����, ����� ������ ����� ����� �������
	_strrev(str);
	for(int i = 1; i <= T[0]; i++) {
		//�������� ����� �-�� �������� ������
		T[i] = str[i-1] - '0';
	}
}

//��������� ������� �����
void out(int *T) {
	for(int i = T[0]; i > 0; i--) {
		printf("%d",T[i]);
	}
}

//��������� ������� ����� + endl
void out_endl(int *T) {
	for(int i = T[0]; i > 0; i--) {
		printf("%d",T[i]);
	}
	printf("\n");
}

//��������� ������� ����� � �����
char* out_string(int *T) {
	char *str;
	int j = 0;
	//��������� ������� �����, ���� ���� �� ������� ������� �����, � ����. ����. ����� ���������� � T[0]
	str = new char[T[0]];

	//����� � ���� ������� ����� (�� ��� ���� �������), � ������� ��. ����� �������� ��� ����� T[i]-�
	for(int i = T[0]; i > 0; i--) {
		str[j++] = T[i] + '0';
	}
	//��������� �����
	str[T[0]] = '\0';
	//��������� ��������
	return str;
}

void add(int *T1, int *T2, int *T3) {
	//��������� ����� ������� �����
	int n = (T1[0]>T2[0])? T1[0] : T2[0];
	int carry = 0, i;
	
	//����� �� ������� ����� � ������ �� ������� ��. ���. ������� ����� ��������� ��. ���. ������� �����
	//+ ������ ������� carry �� ������������ �������
	for(i = 1; i <= n; i++) {
		int temp = T1[i] + T2[i] + carry;
		//�������� ��������� ������ ���������� ������ �� ������ �� 10 ���� ��������� ���� ����
		T3[i] = temp % 10;
		//������� ��������� � ��������� ������
		carry = temp / 10;
	}

	if (carry > 0) {
		n++;
		T3[n] = carry;
	}
	T3[0] = n;
};

int main() {
	/*std::ifstream f1("input.txt");
	std::ofstream f2("output.txt");*/
	FILE *ff1, *ff2;
	ff1 = fopen("input.txt","r");
	ff2 = fopen("output.txt","w");

	int n = 0, j = 0;
	char str[1002], str1[1002], str2[1003], c, *str_res;

	int T[257][1000];
	int k = 0;

	//f1 >> n;
	//std::cout << n;
	//f1 >> c;
	//f1 >> str;
	fscanf(ff1,"%d",&n);

	//������� �� ����� n �����
	for (int i = 1; i <= n+1; i++) {
		//f1 >> str;
		//std::cout << str[0] << std::endl;
		fgets(str,1000,ff1);
		//fgetc(ff1);
		//������ ������ �������� �� ����� ����� ����-������
		str[strlen(str)-1] = '\0';
		printf("%s\n",str);
		//����� �� ������� ��������� �����, � ������ ������ "+"
		for (j = 0; j < strlen(str); j++) {
			//���� ������� �� ��������� ��� �� ���� �� ����� � ����� str1 � ��������� ���� ����-��������, � ��� �� ���� ����� � ����� str2
			if (str[j] == '+') {
				strncpy(str1,str,j);
				str1[j] = '\0';
				strncpy(str2,&(str[j+1]),strlen(str));
				str2[strlen(str2)+1] = '\0';
				//printf("%s \n%s",str1,str2);
				//��� ���������� �� ����� � ���� �����
				strtoarr(str1,T[k]);
				strtoarr(str2,T[k+1]);
				//������ �� ���� �����
				add(T[k],T[k+1],T[k+2]);
			/*	out_endl(T[k]);
				out_endl(T[k+1]);
				out_endl(T[k+2]);*/

				//�������� ����� �����-��������� � �����
				str_res = out_string(T[k+2]);
				//�������� ����� � ����
				//f2 << str_res << endl;
				fprintf(ff2,"%s\n",str_res);
				//cout << str_res << endl;
				//out_endl(T[k-1]);
				//std::cout << str1 << "+" << str2 << std::endl;
				break;
			}
		}
	};
	
	//std::cin.get();
//	getchar();
	/*f1.close();
	f2.close();*/
	fclose(ff1);
	fclose(ff2);
	return 0;
}