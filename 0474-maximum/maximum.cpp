#include <stdio.h>
#include <stdlib.h>

long max = -1000, imax = -1;

//�-� ��������� ���� ���� �����
int sum_digit(long x) {
	char s[20];
	int sum = 0;

	//���������� ����� ���� � �����, �� ������� 10
	ltoa(x,s,10);
	//���� �� ����� ����� �����: �� ���� ������ ����� ����� �����
	for(int i = 0; s[i] != '\0'; i++) {
		sum += (s[i] - 48);
	}
	
	return sum;
}


//�-� ���������� ������������� �����, ��� ����� ���� �������� � � ����� ���� ���� ��������
long maximum(long x) {
	int sum, rank, rang, limit;
	long buf;
	buf = x;
	//��������� (�����������)��������� �����
	for (rank = 0; buf >= 1 ; rank++) {
		buf /= 10;
	}
	
	//printf("%d",rank);
	//getchar();
	rang = 1;
	//�������� ���� �� ���� ������
	for (int i = rank - 1; i != 0 ; i--)
		rang *= 10;
	//rang = (rank-1) * 10;
	//������������ ��� ������, ������ ��� �� ������ ����� � ��� ���� �� ������ �� ����� 99999 ��� �� ���� ������ ����� �� ��������
	limit = rang*0.99;
//	printf("%d\n",limit);

	//���� (�) ����� �� ����� 99999 ������ ������� �� ������ ����� �� ������ �������
	for (int i = x; i >= limit; i--) {
		sum = sum_digit(i);
		if	(sum > max) {
			max = sum;
			imax = i;
		}
//		printf("%d\n",i);
	}
//	getchar();
	return imax;
}


int main() {
	FILE *f1, *f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");
	
	long x, y;
	int s;
	fscanf(f1,"%ld",&x);

	s = sum_digit(x);
	y = maximum(x);
	fprintf(f2,"%ld\n",y);

	fclose(f1);
	fclose(f2);
	return 0;
}