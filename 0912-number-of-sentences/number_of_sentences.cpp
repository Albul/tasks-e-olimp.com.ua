#include <stdio.h>


int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	
	char s[250];
	int c, i = 0;

	//Зчитуємо посимвольно із файла f1, і підраховуємо кількість крапок, знаків оклику і питання (це і буде рівне кількості речень)
	while (!feof(f1)) {
		c = fgetc(f1);
		if (c == 33 || c == 46 || c == 63)
			i++;
	}
		
	fprintf(f2,"%d\n",i);
	fclose(f1);
	fclose(f2);
	return 0;
}

