#include <stdio.h>


int main() {
	FILE *f1, *f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");
	int n, number, sum = 0;
	float price;

	fscanf(f1,"%d",&n);

	for (int i = 1; i <= n; i++) {
		fscanf(f1,"%d%f",&number,&price);
		if (price < 50) 
			sum += number;
	}

	fprintf(f2,"%d\n",sum);

	fclose(f1);
	fclose(f2);
	return 0;
}