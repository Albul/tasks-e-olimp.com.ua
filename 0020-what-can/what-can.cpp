#include <stdio.h>

//Функція яка знаходить суму цифр вхідного числа
int func(int n) {
	char buf[14];
	int sum = 0;
	//Переводимо ціле у рядок
	sprintf(buf,"%d",n);	
	//Сумуємо всі цифри в рядку
	for (int i = 0; i < 14; i++) {
		//Якщо доходимо до кінця рядка, то вийти з циклу підрахунку
		if (buf[i] == 0) 
			break;
			// - 48 дасть нам цифру даного символу
		sum += (buf[i] - 48);
	};
	
	return sum;

};

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	
	int n, sum, count = 0;
	fscanf(f1,"%d",&n);

//	sum = func(n);
//	printf("%d\n",sum);

	//Поки задане число додатнє, вираховуємо суму його цифр і віднімаємо знайдену суму від даного числа, лічильник збільшуємо на одиницю
	while (n > 0) {
		sum = func(n);
		n -= sum;
		count++;
	};
	
	fprintf(f2,"%d\n",count);
	fclose(f1);
	fclose(f2);
	return 0;
}
