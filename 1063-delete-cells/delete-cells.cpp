#include <stdio.h>

char arr[102][102];

void p(int i, int j) {
	arr[i][j] = '.';
	if (arr[i][j+1] == '#')
		p(i,j+1);
	if (arr[i][j-1] == '#')
		p(i,j-1);
	if (arr[i+1][j] == '#')
		p(i+1,j);
	if (arr[i-1][j] == '#')
		p(i-1,j);
}

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	
	int m, n, count = 0;

	fscanf(f1,"%d%d",&m,&n);

	//Зчитуємо останній символ перевода каретки
	fgetc(f1);	
//	putchar(arr[2][0]);	
	for (int i = 0; i < m; i++) {
		fgets(arr[i],102,f1);
//		printf("%s",arr[i]);
	};

	for (int i = 0; i <= m; i++)
		for (int j = 0; j <= n; j++)
			if (arr[i][j] == '#') {
				count++;
				p(i,j);
			};

	//for (int j = 1; j <= n; j++)

	fprintf(f2,"%d\n",count);
//	putchar(arr[1][1]);			

	fclose(f1);
	fclose(f2);
	return 0;
}
