#include <stdio.h>

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");

	float array[4];
	int equal = 0;

	fscanf(f1,"%f%f%f%f",&array[0],&array[1],&array[2],&array[3]);
	
	/* Порівнюємо всі сторони друг із другом, якщо співпадає значення довжини,
	то лічильник збільшуємо на одиницю, при цьому ми не порівнюємо одну й ту
	саму сторону саму із собою. Паралелограм можна утворити якщо є дві пари 
	однакових довжин, або чотирі співпадання. Або ж якщо всі сторони однакові
	це 12 співпадань
	*/
	for (int i = 0; i <= 3; i++)
		for (int j = 0; j <= 3; j++)
			if (array[i] == array[j] && i != j)
				equal++;
	equal -= 2;
	if (equal == 2 || equal == 10) 
		fprintf(f2,"YES\n");
	else
		fprintf(f2,"NO\n");

//	printf("%d",equal);
	//fprintf(f2,"",equal);
	
	fclose(f1);
	fclose(f2);
	return 0;
}
