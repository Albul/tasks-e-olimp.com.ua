#include <iostream>
#include <fstream>
using namespace std;

int main () {
	ifstream inf("input.txt");
	ofstream outf("output.txt");

	int i = 0;
	char ch;
	
	//Зчитати перший символ впусту
	ch = inf.get();
	
	//Поки не кінець файла зчитуємо посимвольно і перевіряємо чи він не є знаком арифметичної операції, якщо так то лічильник збільшини на одиницю
	while (!inf.eof()) {
		ch = inf.get();
		if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
			i++;
	}
	
	outf << i << endl;
	
	inf.close();
	outf.close();
	return 0;
}
