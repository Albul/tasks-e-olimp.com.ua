#include <stdio.h>
#include <memory.h>
#define MAX 102
int m[MAX][MAX], used[MAX], order[MAX], count = 0, tree[MAX][MAX];
int a, b;
int n, r;

void dfs2(int u);

void dfs(int v) {
	int i;
	//������� ������� ����
	used[v] = 1;
	order[count++] = v;

	for (i = 1; i <= n; i++) {
		//���� ���������� �� ������� �������, �� �� ������� ������ ����, ���� ���� ����� ��������� �� �����
		if (m[v][i] && used[i])
			m[v][i] = 0;

		if (m[v][i] && !used[i])
			dfs(i);
	};
	
	//������� ������� ������
	used[v] = 2;
};

int main() {
	memset(m,0,sizeof(m));
	memset(used,0,sizeof(used));

	FILE *f1, *f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");


	fscanf(f1,"%d%d",&n,&r);
	for (int i = 1; i <= r; i++) {
		fscanf(f1,"%d%d",&a,&b);
		m[a][b] = m[b][a] = 1;
	};
	
	dfs2(1);
	
	//�������� �� ����� �� ���������� ���� ��������� �� ����� ��� �����
	for (int i = 1; i <= n; i++) {
		for (int j = i + 1; j <= n; j++) {
			if (tree[i][j])
				fprintf(f2,"%d %d\n",i,j);
		}
	};

	fclose(f1);
	fclose(f2);
	return 0;
}

//����� � �������
void dfs2(int u) {
	used[u]=1;
	
	for (int i = 1; i <= n; i++) {
		if (!used[i] && m[u][i]){
			//������� ������� �������� ������
			tree[u][i] = tree[i][u] = 1; 
			dfs2(i);
		}
	}
	
}
