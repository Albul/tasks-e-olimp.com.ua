#include <stdio.h>

//Злити два масиви, а,b - вказівники на початок масивів, n1, n2 - їхні розміри
int* concat_arrays(int* a, int* b, int n1, int n2) {
	int* numbag12;
	numbag12 = new int[n1+n2];

	int i = 0, j = 0, k = 0;
	while (i < n1)
		numbag12[k++] = a[i++];
	
	while (j < n2)
		numbag12[k++] = b[j++];
	
	return numbag12;
}

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");

	int children, bag1, bag2, numbag1[100], numbag2[100], numbag3[100], bag3, *numbag12;
	
	//Заповнюємо масив нулями, це масив ідентифікатор у ньому будуть одиницями ті його елементи, номер яких збігається із номером дитини якій уже єсть подарунок
	int ar[100] = {0};

	fscanf(f1,"%d%d%d",&children,&bag1,&bag2);
	
	//Зчитуємо номера дітей у першій і другій сумках
	for (int i = 0; i < bag1; i++)
		fscanf(f1,"%d",&numbag1[i]);
	
	for (int i = 0; i < bag2; i++)
		fscanf(f1,"%d",&numbag2[i]);
	
	bag3 = children - (bag1 + bag2);

	//numbag12 - номера дітей яким лежать подарунки у перших двох мішках
	numbag12 = concat_arrays(numbag1,numbag2,bag1,bag2);
	
	//Позначаємо в масиві ar одиницями ті його елементи, номер яких дорівнює номеру дитині якій вже налаштований подарунок
	for (int i = 0; i < (bag1+bag2); i++) {
		ar[numbag12[i]] = 1;
	}
	
	//Йдемо по масиву ar, якщо відповідний елемент == 0, то це значить що дитина з таким номером ще немає подарунка, тому покласти їй подарунок у третій мішок
	int k = 0;
	for (int i = 1; i <= children; i++) {
		if (ar[i] == 0) 
			numbag3[k++] = i;
	}

//	printf("%d",numbag12[2]);

	//Кількість дітей яким потрібно покласти подарунки
	fprintf(f2,"%d\n",bag3);
	//Записати всі елементи масиву крім останнього
	for (int i = 0; i < (bag3-1); i++)
		fprintf(f2,"%d ",numbag3[i]);
	
	//Записати останній елемент масиву і перевести каретку
	fprintf(f2,"%d\n",numbag3[bag3-1]);
	
	fclose(f1);
	fclose(f2);
	return 0;
}
