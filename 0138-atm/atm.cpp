#include <stdio.h>
int atm(int n) {
	int count = 0;
	while (n != 0) {
		if ((n / 500) != 0)	
			n -= 500;
		else
			if ((n/200) != 0)
				n -= 200;
			else
				if ((n / 100) != 0)
					n -= 100;
				else
					if ((n / 50) != 0)
						n -= 50;
					else
						if ((n / 20) != 0)
							n -= 20;
						else
							if ((n / 10) != 0)
								n -= 10;
		count++;
	}
	return count;
}

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	int n,result;
	fscanf(f1,"%d",&n);
	
	if ((n % 10) != 0 )
		fprintf(f2,"-1\n");
	else {
		fprintf(f2,"%d\n",atm(n));
	}

	fclose(f1);
	fclose(f2);
	return 0;
}
