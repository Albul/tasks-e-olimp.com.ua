#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int a,b,c;
	fscanf(inf,"%d %d %d",&a,&b,&c);
	

	//Якщо всі сторони різні то це різносторонній трикутник
	if (a != b && a != c && b != c)
		fprintf(outf,"3\n");
	else {
		//Якщо всі сторони рівні то це рівносторонній трик
		if ( (a == b) && (b == c))
			fprintf(outf,"1\n");
		else
		//В противному випадку це рівнобедренний трикутник
			fprintf(outf,"2\n");
	};
	
	fclose(inf);
	fclose(outf);
	return 0;
}
