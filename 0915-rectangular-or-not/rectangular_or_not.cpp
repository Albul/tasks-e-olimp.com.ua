#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int a,b,c;
	fscanf(inf,"%d %d %d",&a,&b,&c);
	
	//Якщо квадрат гіпотенузи == сумі квадратів катетів, то прямокутний, в противному разі ні
	if (a*a == b*b + c*c) 
		fprintf(outf,"YES\n");
	else {
		if (b*b == a*a + c*c)
			fprintf(outf,"YES\n");
		else
			if (c*c == a*a + b*b)
				fprintf(outf,"YES\n");
			else
				fprintf(outf,"NO\n");
	};
	
	fclose(inf);
	fclose(outf);
	return 0;
}

