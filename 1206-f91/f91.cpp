#include <stdio.h>
int f91(int n) {
	if (n > 100)
		return (n - 10);
	else
		f91(f91(n+11));	
}

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	int n,result;
	fscanf(f1,"%d",&n);
	result = f91(n);
	fprintf(f2,"%d\n",result);
	

	fclose(f1);
	fclose(f2);
	return 0;
}

