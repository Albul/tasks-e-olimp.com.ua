#include <stdio.h>
#define abs(x) ((x < 0)? -x : x)
int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	

	int d1, d2, d3, in, product;

	fscanf(inf,"%d",&in);
	printf("%d",in);
	
	d1 = (int)in / 100;
	d2 = ((int)in / 10) % 10;
	d3 = in % 10;

	product = d1 * d2 * d3;
	fprintf(outf,"%d\n",product);


	fclose(inf);
	fclose(outf);
	return 0;
}
