#include <stdio.h>

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");

	int length, array[100], min = 2147483647, max = -2147483648, sum;
	fscanf(f1,"%d",&length);

	//Зчитуємо масив з файлу і визначаємо мінімальний і максимальний елемент
	for (int i = 0; i < length; i++) {
		fscanf(f1,"%d",&array[i]);
		if (min > array[i])
			min = array[i];
		if (max < array[i])
			max = array[i];

	};
	
	sum = min + max;
	fprintf(f2,"%d\n",sum);

	fclose(f1);
	fclose(f2);
	return 0;
}
