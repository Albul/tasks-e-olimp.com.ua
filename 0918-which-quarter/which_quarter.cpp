#include <stdio.h>

int main() {
	FILE* f1 = fopen("input.txt","r");
	FILE* f2 = fopen("output.txt","w");

	float x,y;
	fscanf(f1,"%f%f",&x,&y);

	if (x > 0 && y > 0)
		fprintf(f2,"1\n");
	else {
		if (x < 0 && y > 0)
			fprintf (f2,"2\n");
		else {
			if (x < 0 && y < 0)
				fprintf(f2,"3\n");
			else {
				if (x > 0 && y < 0)
					fprintf(f2,"4\n");
				else
					fprintf(f2,"0\n");
			};
		};
	};

	fclose(f1);
	fclose(f2);
	return 0;
}
