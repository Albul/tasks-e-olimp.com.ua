#include <stdio.h>
#include <math.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");

	int a,b = 0,c = 0,d;
	int x1,x2;

	fscanf(inf,"%d %d %d",&a,&b,&c);
	d = b*b - 4*a*c;
	//Якщо дискримінант більший або рівний нулю то аналізуєм далі, в противному разі виводимо "No roots\n"
	if (d >= 0) {
		//Один корінь якщо дискримінант == 0
		if (d == 0) {
			x1 =(int) (-b/(2*a));
			fprintf(outf,"One root: %d\n",x1);
		}
		else {
		//Два корні якщо дискримінант більший за нуль
			x1 = (int) ((-b+sqrt(d))/(2*a));
			x2 = (int) ((-b-sqrt(d))/(2*a));
			//Виводимо спочатку менший корінь а потім більший
			if (x1 < x2) 
				fprintf(outf,"Two roots: %d %d\n",x1,x2);
			else
				fprintf(outf,"Two roots: %d %d\n",x2,x1);
		}
	} 
	else
		fprintf(outf,"No roots\n");

	fclose(inf);
	fclose(outf);
	return 0;
}
