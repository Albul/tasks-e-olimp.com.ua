#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int length, array[100], i = 0, number = 0, sum = 0;

	fscanf(inf,"%d",&length);
	//Зчитуємо з файла поки неперевищуємо вказану довжину масиву
	while (!feof(inf)) {
		fscanf(inf,"%d",&array[i]);
		//якщо біжучий елемент при діленні націло на 6 в остачі дає нуль (тобто кратний 6) то додадти його значення до суми і збільшити лічильник number
		if (((array[i] % 6) == 0) && (i < length) && (array[i] != 0)) {
			sum += array[i];
			number++;
		}
		i++;
	};
	
	//%ld - довге ціле в десятковій формі
	fprintf(outf,"%d %d\n",number,sum);

	fclose(inf);
	fclose(outf);
	return 0;
}
