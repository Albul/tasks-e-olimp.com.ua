#include <stdio.h>
#include <memory.h>
#define MAX 101
int m[MAX][MAX], used[MAX], order[MAX], count = 0;
int i, n, k, a ,b, ptr;

void dfs(int v) {
	int i;
	//������� ���������� ��������
	used[v] = 1;
	//³������ � �����, ������� ����������� ���� �������
	order[count] = v;
	count++;
	//������ ������ ��������� �������
	for(i = 1; i <= n; i++)
		//���� ���� ����� �� ���� ������� � ���� �� ������� �� ��������� ���������� dfs ��� ��
		if(m[v][i] && !used[i])
			dfs(i);
	used[v] = 2;
}

int main() {
	//memset(m,0,sizeof(m));
	//memset(used,0,sizeof(used));

	FILE *f1, *f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");

	//������ ���� �������� ��������

	fscanf(f1,"%d %d",&n,&k);

	for (int i = 1; i <= k; i++) {
		fscanf(f1,"%d %d",&a,&b);
		m[a][b] = m[b][a] = 1;
	}
	dfs(1);
	
	count = 0;
	//ϳ��������� ������� ������������� ������ ���� ������ � �������
	for (int i = 1; i <= n; i++) {
		if (used[i] == 2)
			count++;
	}
	
	//���� ���� == ������� ������, �� ���� �������
	if (count == n)
		fprintf(f2,"YES\n");
	else
		fprintf(f2,"NO\n");

	fclose(f1);
	fclose(f2);
	return 0;
}