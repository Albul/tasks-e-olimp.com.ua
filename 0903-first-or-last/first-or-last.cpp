#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE* outf = fopen("output.txt","w");
	
	int x,first,last;
	
	//Зчитуємо із потока inf ціле число і вставляємо його в змінну x (Передаєм покажчик на змінну)
	fscanf(inf,"%d",&x);
	//Знаходимо останню цифру
	last = x % 10;
	//Знаходимо першу цифру
	first = (int)x / 100;
	//Якщо перша більша останньої то записати першу цифру
	if (first > last)
		fprintf(outf,"%d\n",first);	
	else {
		//Якщо вони рівні то записати в поток знак (=)
		if (first == last)
			fprintf(outf,"=\n");
		else
		//В противному разі записати останню цифру
			fprintf(outf,"%d\n",last);
	}
		
	
	//Закрити потокі
	fclose(inf);
	fclose(outf);
	return 0;
}
