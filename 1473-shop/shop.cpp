#include <stdio.h>

int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");
	
	int x,y,z, sum = 0;
	fscanf(f1,"%d%d%d",&x,&y,&z);
	
	if (x >= z) {
		sum += x;
		if (y >= z)
			sum += y;
		else
			sum += z;
	}
	else {
		sum += z;
		if (y >= x)
			sum += y;
		else
			sum += x;
	};
	
	fprintf(f2,"%d\n",sum);

	fclose(f1);
	fclose(f2);
	return 0;
}