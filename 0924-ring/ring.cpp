#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>
//#include <cmath>

int main() {
	FILE *f1,*f2;
	f1 = fopen("input.txt","r");
	f2 = fopen("output.txt","w");
	float s1,r1,s2,r2,s;
	fscanf(f1,"%f%f",&s,&r1);

	s1 = M_PI*r1*r1;
	s2 = s1 - s;
	r2 = sqrt(s2/M_PI);
	
	fprintf(f2,"%.2f\n",r2);

	fclose(f1);
	fclose(f2);
	return 0;
}