#include <stdio.h>
#define abs(a) ((a < 0)? -a : a)

//Знаходження найбільшого спільного дільника
int nsd(int a, int b) {
	int c;
	while (b) {
		c = a % b;
		a = b;
		b = c;
	};
	return abs(a);
}

int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	int n, array[1000], NSD = 0;

	fscanf(f1,"%d",&n);

	for (int i = 0; i < n; i++) {
		fscanf(f1,"%d",&array[i]);
//		printf("%d\n",array[i]);
	}
	
	//Йдемо по масиву чисел, для кожних сусідних чисел знаходимо НСД і записуємо його в наступну клітинку, таким чином при наступному проході ми візьмемо НСД попередніх двох чисел і наступного числа
	for (int i = 0; i < (n - 1); i++) {
		array[i+1] = nsd(array[i],array[i+1]);
		//Якщо біжучий НСД рівен одиниці то і НСД всіх чисел буде рівним одиниці
		if (array[i+1] == 1) {
			NSD = 1;
			break;
		}
	}

	//Якщо НСД не одиниця, то справжній НСД буде у останній комірці масива
	if (NSD != 1)
		NSD = array[n-1];
//	NSD = nsd(array[0],array[1]);
	fprintf(f2,"%d\n",NSD);
	fclose(f1);	
	fclose(f2);
	return 0;
}

