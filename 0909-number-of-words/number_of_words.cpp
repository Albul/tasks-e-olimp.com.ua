#include <stdio.h>


int main() {
	FILE *f1 = fopen("input.txt","r");
	FILE *f2 = fopen("output.txt","w");
	
	char s[250];
	int c, i = 0;

	//Зчитуємо посимвольно із файла f1, і підраховуємо кількість прогалин у ньому
	while (!feof(f1)) {
		c = fgetc(f1);
		if (c == 32)
			i++;
	}
	
	//Кількість слів на одне більша від кількості прогалин
	i++;
	fprintf(f2,"%d\n",i);
	fclose(f1);
	fclose(f2);
	return 0;
}
