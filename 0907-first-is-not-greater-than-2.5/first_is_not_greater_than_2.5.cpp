#include <stdio.h>

int main() {
	FILE *inf = fopen("input.txt","r");
	FILE *outf = fopen("output.txt","w");
	
	int length, j = 0, index, result, ind = 0;
	float array[100];

	fscanf(inf,"%d",&length);
	//Поки не кінець файла читаєм
	while (!feof(inf)) {
		fscanf(inf,"%f",&array[j]);
		//Якщо біжучий елемент масиву не більший за 2.5, і довжина менша за довжину масиву, то записати його порядковий елемент і саме значення у вихідний файл (значення із точністю 2)
		if ((array[j] <= 2.5) && (j < length)) {
			fprintf(outf,"%d %.2f\n",j+1,array[j]);
			ind = 1;
			break;
		};
		j++;
	}
	
	//Якщо не було знайдено жодного числа то вивести повідомлення
	if (ind == 0) 
		fprintf(outf,"Not Found\n");

	fclose(inf);
	fclose(outf);
	return 0;
}
